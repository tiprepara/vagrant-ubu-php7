Vagrant-ubu-php7
===========

** PODE SER UTILIZADO EM AMBOS SISTEMAS: WINDOWS E LINUX **

Servidor LAMP (UBUNTU/TRUSTY64, Apache, MySQL, PHP7)


Configuracão do Vagrant para criar uma máquina virtual (Ubuntu Server 64 Bits) pronta
para desenvolvimento em PHP.

### Packages:

- PHP 7.1
- MySQL 5.5
- Git
- PhpMyAdmin 
- Composer
- cURL
- Vim
- Redis

** Qualquer dúvida, consultar o arquivo provision.sh **

Softwares não inclusos: 
==============

- Virtualbox - https://www.virtualbox.org/
- Vagrant - http://www.vagrantup.com/
- Git - http://git-scm.com ( É importante para que você possa criar e configurar seu proprio projeto).

Voce precisará:
==============

- Acesso a Internet

Três passos para a utilização:
========================

1º -> Com o Git Instalado em  sua máquina física:

* Clone esse repositório para sua máquina:

- git clone https://tiprepara@bitbucket.org/tiprepara/vagrant-ubu-php7.git

* Rode o comando:

- vagrant up 


2º -> Sem o Git instalado, acesse https://tiprepara@bitbucket.org/tiprepara/vagrant-ubu-php7.git e faça o download do arquivo.zip e descompacte-o.

* Rode o comando:

- vagrant up


3° -> O Ubuntu Server 64 será baixado e configurado na máquina virtual no VirtualBox através do provisionador BASH: 'provision.sh'.

Depois de pronto, o servidor web Apache estará disponível no endereço http://localhost:8084 ou http://192.168.33.11. E para o PhpmyAdmin:
http://localhost:8084/phpmyadmin ou assim: http://192.168.33.11/phpmyadmin. E verifique o php através de http://localhost:8084/phpinfo.php ou http://192.168.33.11/phpinfo.php.

*Para acessar o phpmyadmin	utilize:

- Login: root
- Senha: vagrant

** FAÇA ISTO CASA HAJA ALGUM CONFLITO EM SUA MÁQUINA E NÃO ACESSE O PHPINFO.PHP DIRETAMENTE **

- Ja logado na VM, crie um arquivo php no diretório html: phpinfo.php:

*<?php phpinfo(); ?>

- Salve-o e agora sim deverá funcionar: http://localhost:8084/phpinfo.php 

O diretório www será o responsavél pelo compartilhamento entre a máquina física e a virtual. E crie seus projetos dentro da
pasta html/.

** Caso haja necessidade, seguir estes passos:

- 1° vagrant plugin install vagrant-vbguest 
- 2° vagrant plugin install vagrant-winnfsd 
- 3° Logado na VM: 
*$ sudo ln -s /opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions
- 4° Nao esquecer de executar: vagrant reload
- Porém, acreditamos que com o plugin instalado nao serão necessários esses passos acima.

Desejamos um bom desenvolvimento e diversão!




